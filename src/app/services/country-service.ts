import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor( private http: HttpClient) { }

  getCounties(city) {
    console.log(city);
    return this.http.get('http://localhost:9091/soap?country=' + city);
  }
}
