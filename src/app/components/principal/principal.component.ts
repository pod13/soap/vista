import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CountryService } from '../../services/country-service';


@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {

  form: FormGroup;
  capital = '';
  divisa = '';
  nombre = '';
  poblacion = '';

  constructor( private formBuilder: FormBuilder, private countryService: CountryService ) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.form = this.formBuilder.group({
      pais : ['', [Validators.required, Validators.minLength(4)]]
    });
  }

  find() {
    if (this.form.invalid) {
      return Object.values(this.form.controls).forEach( control => {
        control.markAsTouched();
      });
    } else {
      const country = this.form.controls['pais'].value;
      this.countryService.getCounties(country).subscribe((data: any) => {
        console.log(data);
        this.nombre = data.name;
        this.divisa = data.currency;
        this.capital = data.capital;
        this.poblacion = data.population;

      }, error => {
          console.error('Error ws soap');
          this.nombre = 'No existe dato';
          this.divisa = 'No existe dato';
          this.capital = 'No existe dato';
          this.poblacion = 'No existe dato';
      });

    }

  }

}
